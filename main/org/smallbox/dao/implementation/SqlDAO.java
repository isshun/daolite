/*
 * This file is part of DAOLite.
 *
 *     SPACrawler is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     DAOLite is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with DAOLite.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.smallbox.dao.implementation;

import org.smallbox.dao.BaseModel;
import org.smallbox.dao.DAO;

public abstract class SqlDAO<T extends BaseModel> extends DAO<T> {

    public SqlDAO() {
        createTable();
    }

    @Override
    public T create(T obj) {
        insert(obj);
        return obj;
    }

    public abstract void createTable();
    protected abstract void insert(T obj);
}
