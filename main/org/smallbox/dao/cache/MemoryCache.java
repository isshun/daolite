/*
 * This file is part of DAOLite.
 *
 *     SPACrawler is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     DAOLite is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with DAOLite.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.smallbox.dao.cache;

import org.smallbox.dao.BaseModel;
import org.smallbox.dao.DAO;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MemoryCache<T extends BaseModel> extends DAO<T> {
    private final DAO<T>            _boxedDAO;
    private final List<T>           _objects;
    private final Map<String, T>    _objectsByUUID;

    public MemoryCache(DAO<T> boxed) {
        _boxedDAO = boxed;
        _objects = new ArrayList<>();

        List<T> objects = boxed.all();
        if (objects != null) {
            _objects.addAll(objects);
        }

        _objectsByUUID = _objects.stream().collect(Collectors.toMap(BaseModel::getUUID, object -> object));
    }

    @Override
    public List<T> all() {
        return _objects;
    }

    @Override
    public T find(String uuid) {
        return _objectsByUUID.get(uuid);
    }

    @Override
    public T create(T obj) {
        _boxedDAO.create(obj);
        _objects.add(obj);
        _objectsByUUID.put(obj.getUUID(), obj);
        return obj;
    }

    @Override
    public void update(T obj) {
        _boxedDAO.update(obj);
    }

    @Override
    public void delete(T obj) {
        _boxedDAO.delete(obj);
        _objects.remove(obj);
        _objectsByUUID.remove(obj.getUUID());
    }

    @Override
    public T refresh(T obj) {
        // Remove current object
        _objects.remove(obj);
        _objectsByUUID.remove(obj.getUUID());

        // Get new one from boxed DAO
        obj = _boxedDAO.find(obj.getUUID());
        _objects.add(obj);
        _objectsByUUID.put(obj.getUUID(), obj);

        return obj;
    }
}
