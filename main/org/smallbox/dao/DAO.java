/*
 * This file is part of DAOLite.
 *
 *     SPACrawler is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     DAOLite is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with DAOLite.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.smallbox.dao;

import java.util.List;

public abstract class DAO<T extends BaseModel> {
    /**
     * Get all objects from DB
     * @return Objects list
     */
    public abstract List<T> all();

    /**
     * Get object by UUID
     * @param uuid
     * @return Object matching uuid
     */
    public abstract T find(String uuid);

    /**
     * Create new entry in DB for object
     * @param obj
     * @return New created object
     */
    public abstract T create(T obj);

    /**
     * Update DB entry for object
     * @param obj
     * @return Updated object
     */
    public abstract void update(T obj);

    /**
     * Delete object from DB
     * @param obj
     */
    public abstract void delete(T obj);

    /**
     * Refresh object from DB
     * @param obj
     */
    public abstract T refresh(T obj);
}
