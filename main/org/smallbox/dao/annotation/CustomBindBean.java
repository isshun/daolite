/*
 * This file is part of DAOLite.
 *
 *     SPACrawler is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     DAOLite is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with DAOLite.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.smallbox.dao.annotation;

import org.skife.jdbi.v2.SQLStatement;
import org.skife.jdbi.v2.sqlobject.Binder;
import org.skife.jdbi.v2.sqlobject.BinderFactory;
import org.skife.jdbi.v2.sqlobject.BindingAnnotation;
import org.smallbox.dao.BaseModel;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.annotation.*;
import java.lang.reflect.Method;

@BindingAnnotation(CustomBindBean.SomethingBinderFactory.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PARAMETER})
public @interface CustomBindBean {
    String value() default "___jdbi_bare___";

    class SomethingBinderFactory implements BinderFactory {
        public Binder build(Annotation annotation) {
            return new Binder<CustomBindBean, Object>() {
                public void bind(SQLStatement q, CustomBindBean bind, Object arg) {
                    final String prefix = "___jdbi_bare___".equals(bind.value()) ? "" : bind.value() + "";

                    try {
                        BeanInfo info = Introspector.getBeanInfo(arg.getClass());
                        PropertyDescriptor[] props = info.getPropertyDescriptors();
                        for (PropertyDescriptor prop : props) {
                            Method readMethod = prop.getReadMethod();
                            if (readMethod != null) {
                                Object r = readMethod.invoke(arg);
                                Class<?> c = readMethod.getReturnType();
                                if (r instanceof BaseModel) {
                                    r = ((BaseModel)r).getUUID();
                                    c = r.getClass();
                                }
                                q.dynamicBind(c, prefix + prop.getName(), r);
                            }
                        }
                    } catch (Exception e) {
                        throw new IllegalStateException("unable to bind bean properties", e);
                    }
                }
            };
        }
    }
}
