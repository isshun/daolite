/*
 * This file is part of DAOLite.
 *
 *     SPACrawler is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     DAOLite is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with DAOLite.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.smallbox.dao;

import java.util.UUID;

public abstract class BaseModel {
    private String    _uuid = UUID.randomUUID().toString();

    public String           getUUID() { return _uuid; }
    public void             setUUID(String uuid) { _uuid = uuid; }
}